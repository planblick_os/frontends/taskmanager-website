#!/bin/sh
_term() {
  echo "Caught SIGTERM signal!"
}

_termint() {
  echo "Caught SIGINT signal!"
}

trap _term SIGTERM
trap _termint SIGINT

nginx